<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Cliente;
use App\Repository\ClienteRepository;
use App\Form\ClienteType;
use App\Form\ClienteTypeEdit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ClienteController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function read(ClienteRepository $clienteRepository)
    {
        $clientes = $clienteRepository
            ->findAll();
        
        
        return $this->render('cliente.html.twig', [
            'clientes' => $clientes
        ]);
    }

    #[Route('/create', name: 'cliente-create')]
    public function create(Request $request, ManagerRegistry $doctrine): Response {
        
        $entityManager = $doctrine->getManager();

        $cliente = new Cliente();

        $form = $this->createForm(ClienteType::class, $cliente);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $telefone = str_replace(['(', ')', ' ', '-'], ['', '', '', ''], $form->get('telefone')->getData());
            
            $cliente->setTelefone($telefone);

            if(!$form->get('notif_whats')->getData()){
                $cliente->setNotifWhats(false);
            } 

            if(!$form->get('emails_promocionais')->getData()){
                $cliente->setEmailsPromocionais(false);
            } 

            $entityManager->persist($cliente);

            $cliente = $form->getData();
           
            $entityManager->persist($cliente);
            $entityManager->flush();
            
            $this->addFlash(
                'success',
                'Cliente cadastrado com sucesso!'
            );
            
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('criarCliente.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/update/{id}', name: 'cliente-update')]
    public function update(int $id, Request $request, ManagerRegistry $doctrine, ClienteRepository $clienteRepository): Response {
        $entityManager = $doctrine->getManager();
        $cliente = $entityManager->getRepository(Cliente::class)->find($id);

        if(!$cliente){
            $this->addFlash(
                'error',
                'Esse cliente não existe. :('
            );
            
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(ClienteTypeEdit::class, $cliente);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $telefone = str_replace(['(', ')', ' ', '-'], ['', '', '', ''], $form->get('telefone')->getData());
            
            $cliente->setTelefone($telefone);

            if(!$form->get('notif_whats')->getData()){
                $cliente->setNotifWhats(false);
            } 

            if(!$form->get('emails_promocionais')->getData()){
                $cliente->setEmailsPromocionais(false);
            } 

            $entityManager->persist($cliente);

            $cliente = $form->getData();
            $entityManager->persist($cliente);
            $entityManager->flush();
            

            $this->addFlash(
                'success',
                'O cliente foi editado com sucesso!'
            );
            
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('editCliente.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/delete/{id}', name: 'cliente-delete')]
    public function delete(int $id, Request $request, ManagerRegistry $doctrine, ClienteRepository $clienteRepository): Response {

        $entityManager = $doctrine->getManager();
        $cliente = $entityManager->getRepository(Cliente::class)->find($id);

        if(!$cliente){
            $this->addFlash(
                'error',
                'Esse cliente não existe. :('
            );
            
            return $this->redirectToRoute('home');
        }

            $this->addFlash(
                'success',
                'O cliente foi deletado com sucesso!'
            );

            $entityManager->remove($cliente);
            $entityManager->flush();
            
            
            return $this->redirectToRoute('home');
    }

}