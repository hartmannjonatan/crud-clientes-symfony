<?php

namespace App\Entity;

use App\Repository\ClienteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ClienteRepository::class)]
class Cliente
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 150)]
    private $nome;

    #[ORM\Column(type: 'string', length: 20)]
    private $genero;

    #[ORM\Column(type: 'string', length: 14)]
    private $cpf;

    #[ORM\Column(type: 'bigint')]
    private $telefone;

    #[ORM\Column(type: 'string', length: 50)]
    private $senha;

    #[ORM\Column(type: 'boolean')]
    private $notif_whats;

    #[ORM\Column(type: 'boolean')]
    private $emails_promocionais;

    #[ORM\Column(type: 'string', length: 100)]
    private $email;

    #[ORM\Column(type: 'date')]
    private $dataNasc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getGenero(): ?string
    {
        return $this->genero;
    }

    public function setGenero(string $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    public function getSenha(): ?string
    {
        return $this->senha;
    }

    public function setSenha(string $senha): self
    {
        $this->senha = $senha;

        return $this;
    }

    public function getNotifWhats(): ?bool
    {
        return $this->notif_whats;
    }

    public function setNotifWhats(bool $notif_whats): self
    {
        $this->notif_whats = $notif_whats;

        return $this;
    }

    public function getEmailsPromocionais(): ?bool
    {
        return $this->emails_promocionais;
    }

    public function setEmailsPromocionais(bool $emails_promocionais): self
    {
        $this->emails_promocionais = $emails_promocionais;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDataNasc(): ?\DateTimeInterface
    {
        return $this->dataNasc;
    }

    public function setDataNasc(\DateTimeInterface $dataNasc): self
    {
        $this->dataNasc = $dataNasc;

        return $this;
    }
}
